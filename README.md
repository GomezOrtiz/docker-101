# **DOCKER 101** --- (By David G. Ortiz)
## Built your first dockerized Vue + Spring Boot app

### AVAILABLE COMMANDS

To build and run DB, backend and frontend in containers, run one ot these:

```
docker-compose up
```

```
make start
```

There are also different commands in Makefile to run only DB, backend or frontend.

To start the slideshow in terminal, run:

```
make slides
```

Note that you should have installed this tool: https://github.com/vinayak-mehta/present. You can do it by simple running:

```
pip install present
```