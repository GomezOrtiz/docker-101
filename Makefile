start: start-front start-back

start-front: build-front
	@docker run --rm -d -p 3000:80 \
	--name realworldapp_frontend realworldapp_frontend

#### TO RUN ONE STAGE VERSION (not recommended)
# start-front: build-front
# 	@docker run --rm -d -p 3000:8080 \
# 	--name realworldapp_frontend realworldapp_frontend

build-front: 
	@docker build -t realworldapp_frontend ./frontend

start-back: build-back
	@docker run --rm -d -p 8080:8080 --network backend \
	-e SPRING_DATASOURCE_URL=jdbc:postgresql://realworldapp_db:5432/realworld \
	--name realworldapp_backend realworldapp_backend

build-back: start-db ping-db
	@docker build -t realworldapp_backend ./backend

ping-db:
	@docker exec realworldapp_db pg_isready --dbname=realworld --host=localhost --port=5432 --username=realworld

start-db: create-network
	@docker run --rm -d -p 5432:5432 --network backend \
	-e POSTGRES_DB=realworld -e POSTGRES_USER=realworld -e POSTGRES_PASSWORD=r34lw0rld \
	-v postgres-data:/var/lib/postgresql/data --name realworldapp_db postgres:12-alpine

create-network:
	@docker network create -d bridge backend

stop:
	@docker stop realworldapp_frontend realworldapp_backend realworldapp_db

clean:
	@docker network rm backend

slides:
	@present slides/slides.md

.PHONY: start start-front start-back start-db build-back build-front ping-db stop clean slides slides2