<!-- fg=black bg=blue -->

**DOCKER 101** --- (By David G. Ortiz)

```
                    ##        .            
              ## ## ##       ==            
           ## ## ## ##      ===            
       /""""""""""""""""\___/ ===        
  ~~~ {~~ ~~~~ ~~~ ~~~~ ~~ ~ /  ===- ~~~   
       \______ o          __/            
         \    \        __/             
          \____\______/                
```
## Built your first dockerized Vue + Spring Boot app

---
<!-- fg=black bg=blue -->

## WHAT IS DOCKER?

Docker is an open platform for developing, shipping, and running applications. 

Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. 

With Docker, you can significantly reduce the delay between writing code and running it in production.

Docker containers can run on a developer’s local laptop, on physical or virtual machines in a data center, on cloud providers, or in a mixture of environments.

---
<!-- fg=black bg=blue -->

## THE BASICS: DAEMON AND CLIENT

Docker uses a client-server architecture. 

The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. 

The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.

---
<!-- fg=black bg=blue -->

### DAEMON

The Docker daemon (dockerd) listens for Docker API requests and manages Docker objects such as images, containers, networks, and volumes.

TIP: To see real time logs from the server, run:

```
docker events
```

---
<!-- fg=black bg=blue -->

### CLIENT

The Docker client (docker) is the primary way that many Docker users interact with Docker. 

When you use commands such as docker run, the client sends these commands to dockerd, which carries them out. 

---
<!-- fg=black bg=blue -->

## THE BASICS: DOCKER OBJECTS

When you use Docker, you are creating and using images, containers, networks, volumes, plugins, and other objects.

---
<!-- fg=black bg=blue -->

### IMAGES

An image is a read-only template with instructions for creating a Docker container. 

Often, an image is based on another image, with some additional customization. 

```
docker images
```

---
<!-- fg=black bg=blue -->

### CONTAINERS

A container is a runnable instance of an image. 

A container is defined by its image as well as any configuration options you provide to it when you create or start it. 

When a container is removed, any changes to its state that are not stored in persistent storage disappear.

```
docker ps -a
```

---
<!-- fg=black bg=blue -->

## THE BASICS: REGISTRIES

A Docker registry stores Docker images.  When you use the **docker pull** or **docker run** commands, the required images are pulled from your configured registry. 

When you use the **docker push** command, your image is pushed to your configured registry.

[Docker Hub](https://hub.docker.com/) is a public registry that anyone can use, and Docker is configured to look for images on Docker Hub by default.

```
docker pull ubuntu
```

---
<!-- fg=black bg=blue -->

## YOUR FIRST CONTAINER

```
docker run -it ubuntu /bin/bash
```

---
<!-- effect=matrix -->

---
<!-- fg=black bg=blue -->

# REALWORLD 

You are about to dockerize RealWorld... O_o

https://demo.realworld.io/

RealWorld shows you how the exact same real world blogging platform is built using React/Angular/& more on top of Node/Django/& more. 

### FRONTEND: VUE
https://github.com/gothinkster/vue-realworld-example-app

### BACKEND: SPRING BOOT
https://github.com/gothinkster/spring-boot-realworld-example-app

---
<!-- fg=black bg=blue -->

## FIRST STEP: Dockerizing Postgres

Note that you should replace default H2 in-memory database for a Postgres datasource. Involved steps:

- Update application.properties with Postgres
- Create /test/resources/application.properties without Postgres
- Include Postgres dependency in build.gradle
- Fix incompatible limit/offset usage in ArticleReadService

```
	docker run --rm -p 5432:5432 \
	-e POSTGRES_DB=realworld -e POSTGRES_USER=realworld -e POSTGRES_PASSWORD=r34lw0rld \
	-v postgres-data:/var/lib/postgresql/data --name realworldapp_db postgres:12-alpine
```

---
<!-- fg=black bg=blue -->

## WHAT'S A DOCKERFILE?

Docker can build images automatically by reading the instructions from a Dockerfile. 

A Dockerfile is a text document that contains all the commands a user could call on the command line to assemble an image. 

Using docker build users can create an automated build that executes several command-line instructions in succession.


---
<!-- fg=black bg=blue -->

## SECOND STEP: DOCKERIZING FRONTEND

Note that you should point to the right backend URL in /src/common/api.service.js (you could use an .env file or any other suitable approach)

```
FROM node:lts-alpine
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 8080
CMD [ "npm", "run", "serve" ]
```

```
docker build -t realworldapp_frontend ./frontend
```

```
docker run --rm -p 3000:8080 --name realworldapp_frontend realworldapp_frontend
```

---
<!-- fg=black bg=blue -->

## THIRD STEP: DOCKERIZING BACKEND

You can safely remove docker-compose.yml from backend, since we are going to create our own later.

```
FROM openjdk:8-jdk-alpine

WORKDIR /app
COPY . .
RUN ./gradlew clean build --no-daemon 
COPY /build/libs/*.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]
```

```
docker build -t realworldapp_backend ./backend
```

---
<!-- fg=black bg=blue -->

## FOURTH STEP: CREATE BACKEND NETWORK

For your backend to be able to connect to Postgres (which is running in a different container), you should create a Docker network. 

```
docker network create -d bridge backend
```

```
docker run --rm -p 5432:5432 --network backend \
	-e POSTGRES_DB=realworld -e POSTGRES_USER=realworld -e POSTGRES_PASSWORD=r34lw0rld \
	-v postgres-data:/var/lib/postgresql/data --name realworldapp_db postgres:12-alpine
```

```
docker run --rm -p 8080:8080 --network backend \
	-e SPRING_DATASOURCE_URL=jdbc:postgresql://realworldapp_db:5432/realworld \
	--name realworldapp_backend realworldapp_backend
```

---
<!-- effect=explosions -->

### TIME TO COMPOSE!!

---
<!-- fg=black bg=blue -->

## WHAT IS DOCKER COMPOSE?

Compose is a tool for defining and running multi-container Docker applications. 

With Compose, you use a YAML file to configure your application’s services. 

Then, with a single command, you create and start all the services from your configuration.

---
<!-- fg=black bg=blue -->

## WHY SHOULD I USE DOCKER COMPOSE?

### Development environments

Compose can reduce a multi-page “developer getting started guide” to a single machine readable Compose file and a few commands.

### Automated testing environments

Compose provides a convenient way to create and destroy isolated testing environments in the context of a CI/CD pipeline. Specially useful to run end-to-end or integration tests.

### Single host deployments

Compose has traditionally been focused on development and testing workflows, but you can use Compose to deploy to a remote Docker Engine.

---
<!-- fg=black bg=blue -->

### FIRST STEP: Database

```
  db:
    image: postgres:12-alpine
    ports:
      - 5432:5432
    volumes:
      - postgres-data:/var/lib/postgresql/data
    networks:
      - backend
      - admin
    environment:
      - POSTGRES_DB=realworld
      - POSTGRES_USER=realworld
      - POSTGRES_PASSWORD=r34lw0rld
    restart: unless-stopped
```

---
<!-- fg=black bg=blue -->

### SECOND STEP: Backend

```
  app_backend:
    build: ./backend
    image: realworldapp_backend
    ports: 
      - 8080:8080
    networks:
      - backend
    environment:
      - SPRING_DATASOURCE_URL=jdbc:postgresql://db:5432/realworld
    depends_on: 
      - db
    restart: on-failure:3
```

---
<!-- fg=black bg=blue -->

### THIRD STEP: Frontend

```
  app_frontend:
    build: ./frontend
    image: realworldapp_frontend
    ports: 
      - 3000:80
    depends_on: 
      - app_backend
    restart: on-failure:3
```

---
<!-- fg=black bg=blue -->

### BONUS: Adminer

```
  adminer:
    image: adminer:4.7
    ports:
      - 8081:8080
    networks:
      - admin
    depends_on:
      - db
    restart: unless-stopped
```

---
<!-- effect=fireworks -->

### THANK YOU!!